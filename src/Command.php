<?php

namespace CPTeam\Resolver;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Command extends \Symfony\Component\Console\Command\Command
{
	protected function configure()
	{
		parent::configure();
		
		$this->setName("command")
			->setDescription("Resolver command");
	}
	
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		
		$output->write("Hi there, resolver command");
		
	}
	
}
